package INF101.lab2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> itemList = new ArrayList<FridgeItem>();
    int capacity = 20;

    // methods

    @Override
    public int nItemsInFridge() {
        return itemList.size();
    }

    @Override
    public int totalSize() {
        return capacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemList.size() < capacity) {
            itemList.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemList.contains(item)) {
            itemList.remove(item);
        } else {
            throw new NoSuchElementException();
        }

    }

    @Override
    public void emptyFridge() {
        itemList.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() { // feiler
        List<FridgeItem> retList = new ArrayList<FridgeItem>();
        for (FridgeItem item : itemList) {
            if (item.hasExpired() == true) {
                retList.add(item);
            }
        }
        itemList.removeAll(retList);
        return retList;
    }
}
